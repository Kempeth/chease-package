<?php
namespace Chease
{
	class BootstrapBase
	{
		/**
		 * Called when the system bootstrapper has finished.
		 * (And before any other custom bootstrapper methods are called.)
		 */
		public function OnInit()
		{
		}
		
		/**
		 * Called before a request is handled
		 */
		public function OnRequest()
		{
		}
		
		
		/**
		 * Called before a layout is rendered
		 * 
		 * @param \Chease\Layout $layout
		 */
		public function OnLayout(\Chease\Layout $layout)
		{
			
		}
	}
}