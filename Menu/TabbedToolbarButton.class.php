<?php
namespace Chease\Menu
{
	class TabbedToolbarButton
	{
		private $label = '???';
		private $tooltip = '';
		private $size = 'small';
		private $icon = '';
		private $name = '';
		private $value = '';
		private $href = '';
		private $disabled = false;

		/**
		 * Gets or sets the label of this button.
		 *
		 * @param string $label
		 * @return string
		 */
		public function Label($label = null)
		{
			if ($label != null)
			{
				$this->label = $label;
			}
			return $this->label;
		}

		/**
		 * Gets or sets the tool tip of this button.
		 *
		 * @param string $tooltip
		 * @return string
		 */
		public function Tooltip($tooltip = null)
		{
			if ($tooltip != null)
			{
				$this->tooltip = $tooltip;
			}
			return $this->tooltip;
		}

		/**
		 * Gets or sets the size of this button.
		 *
		 * @param string $size
		 * @return string
		 */
		public function Size($size = null)
		{
			if ($size != null)
			{
				$this->size = $size;
			}
			return $this->size;
		}

		/**
		 * Gets or sets the icon of this button.
		 *
		 * @param string $icon
		 * @return string
		 */
		public function Icon($icon = null)
		{
			if ($icon != null)
			{
				$this->icon = $icon;
			}
			return $this->icon;
		}

		/**
		 * Gets or sets the name of this button.
		 *
		 * @param string $name
		 * @return string
		 */
		public function Name($name = null)
		{
			if ($name != null)
			{
				$this->name = $name;
				$this->href = null;
			}
			return $this->name;
		}

		/**
		 * Gets or sets the value of this button.
		 *
		 * @param string $value
		 * @return string
		 */
		public function Value($value = null)
		{
			if ($value != null)
			{
				$this->value = $value;
				$this->href = null;
			}
			return $this->value;
		}

		/**
		 * Gets or sets the href of this button.
		 *
		 * @param string $href
		 * @return string
		 */
		public function HRef($href = null)
		{
			if ($href != null)
			{
				$this->href = $href;
				$this->name = null;
				$this->value = null;
			}
			return $this->href;
		}

		/**
		 * Gets or sets whether the button is disabled.
		 *
		 * @param boolean $disabled
		 * @return boolean
		 */
		public function Disabled($disabled = null)
		{
			if ($disabled != null)
			{
				$this->disabled = $disabled;
			}
			return $this->disabled;
		}

		public function ToHtml()
		{
			$attr = [];
			$classes = [];
			if ($this->href != null)
			{
				$tag = 'a';
				$classes[] = 'button';
				$attr[] = "href=\"" . APP . "{$this->href}\"";
			}
			else
			{
				$tag = 'button';
				$attr[] = "name=\"{$this->name}\"";
				$attr[] = "value=\"{$this->value}\"";
			}
			$attr[] = "title=\"{$this->tooltip}\"";
			if ($this->disabled) $attr[] = "disabled";
			$classes[] = $this->size;
				
			return "<$tag class=\"" . implode(' ',	$classes)    . "\" " . implode(' ', $attr) . ">"
					."<img src=\"" . APP . "{$this->icon}\" /> <span>{$this->label}</span>"
					."</$tag>";
		}
	}
}