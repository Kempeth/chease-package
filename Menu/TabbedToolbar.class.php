<?php
namespace Chease\Menu
{
	class TabbedToolbar
	{
		private $tabs = [];
		private $active = null;
		
		public function AddTab(TabbedToolbarTab $tab = null)
		{
			if ($tab == null)
			{
				//echo "create new tab";
				$tab = new TabbedToolbarTab();
			}
			//else echo "adding existing tab";
			$this->tabs[] = $tab;
			
			return $tab;
		}
		
		public function NewTab()
		{
			return new TabbedToolbarTab();
		}
		
		public function ToHtml()
		{
			$html = "<ul tabbedtoolbar>";
			
			if (count($this->tabs) > 0)
			{
				if ($this->active == null)
				{
					$this->active = $this->tabs[0];
				}
				$this->active->Active(true);
			}
			
			foreach ($this->tabs as $tab)
			{
				$html .= $tab->ToHtml();
			}
			
			$html .= "</ul>";
			
			return $html;
		}
		
		public function SetActiveTab(TabbedToolbarTab $tab)
		{
			$this->active = $tab;
		}
	}
}