<?php
namespace Chease\Menu
{
	class TabbedToolbarTab
	{
		private $name = '???';
		private $groups = [];
		private $active = false;

		/**
		 * Gets or Sets the name of this tab.
		 * 
		 * @param string $name
		 * @return string
		 */
		public function Name($name = null)
		{
			if ($name != null)
			{
				$this->name = $name;
			}
			return $this->name;
		}
		
		/**
		 * Gets or sets whether or not this tab is the active tab.
		 * 
		 * @param boolean $active
		 * @return boolean
		 */
		public function Active($active = null)
		{
			if ($active != null)
			{
				$this->active = $active;
			}
			return $this->active;
		}

		public function AddGroup(TabbedToolbarGroup $group = null)
		{
			if ($group == null)
			{
				$group = new TabbedToolbarGroup();
			}
			$this->groups[] = $group;
			return $group;
		}

		public function NewGroup()
		{
			return new TabbedToolbarGroup();
		}

		public function ToHtml()
		{
			$html = "<li toolbartab>";
			$html .= "<input type=\"radio\" id=\"tab{$this->name}\" name=\"tabs\"" . ($this->active ? ' checked' : '') . " />";
			$html .= "<label for=\"tab{$this->name}\">{$this->name}</label>";
			$html .= "<ul buttongroups>";
				
			foreach ($this->groups as $group)
			{
				$html .= $group->ToHtml();
			}
				
			$html .= "</ul></li>";
				
			return $html;
		}
	}
}