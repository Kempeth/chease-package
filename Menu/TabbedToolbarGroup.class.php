<?php
namespace Chease\Menu
{
	class TabbedToolbarGroup
	{
		private $name = '???';
		private $buttons = [];

		/**
		 * Gets or Sets the name of this group.
		 *
		 * @param string $name
		 * @return string
		 */
		public function Name($name = null)
		{
			if ($name != null)
			{
				$this->name = $name;
			}
			return $this->name;
		}

		public function AddButton(TabbedToolbarButton $button = null)
		{
			if ($button == null)
			{
				$button = new TabbedToolbarButton();
			}
			$this->buttons[] = $button;
			return $button;
		}

		public function NewButton()
		{
			return new TabbedToolbarButton();
		}

		public function ToHtml()
		{
			$html = '';
				
			$smallcount = 0;
			foreach ($this->buttons as $button)
			{
				if ($button->Size() == 'large' ||
						$lastsize == 'large' ||
						($button->Size() == 'small' && $smallcount == 3) ||
						!isset($lastsize))
				{
					if (isset($lastsize))
					{
						//close existing column
						$html .= '</li>';
					}
					//start new column
					$html .= '<li buttoncol>';
					$smallcount = 0;
				}

				$smallcount++;
				$lastsize = $button->Size();

				$html .= $button->ToHtml();
			}
				
			if (strlen($html) > 0)
			{
				$html .= '</li>';
			}
				
			$html = "<li buttongroup><ul buttons>$html</ul><label>{$this->name}</label></li>";
			return $html;
		}
	}
}