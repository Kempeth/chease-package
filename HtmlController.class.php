<?php
namespace Chease
{
	/**
	 * An HtmlController allows serving views on any action that is undefined. This allows simple content to be served very easily inside the
	 * Controller-Action paradigm.
	 *
	 * @author dschachtler
	 */
	class HtmlController extends Controller
	{
		protected function performRouteInternal()
		{
			$methodname = 'action' . ucfirst($this->route->getAction());
			if (method_exists($this, $methodname))
			{
				call_user_func(array($this, $methodname));
			}
		}
	}
}