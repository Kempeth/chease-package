<?php
//define('DEBUG', true);

// Determine whether the server is configured to serve the "public" folder and set our base for building absolute web paths
$root = str_replace("\\", "/", $_SERVER['DOCUMENT_ROOT']);
$dir = str_replace("\\", "/", dirname($_SERVER["SCRIPT_FILENAME"]));
$web_dir = str_replace($root, '', $dir) . '/';
define("APP", $web_dir);


// Determine whether the browser navigated to a location outside the "public" folder and redirect to index
if (strcasecmp(substr($_SERVER["REQUEST_URI"], 0, strlen(APP)), APP) !== 0)
{
	header("Location: " . APP);
	die;
}


// Wire up autoloaders
require_once('autoload.php');


// Load Configuration
\Chease\Config::Load();


// Run custom bootstrap
\Chease\Config::Bootstrap()->OnInit();
\Chease\Config::Bootstrap()->OnRequest();


// Establish and follow route
$request = isset($_GET['_url']) ? $_GET['_url'] : '';
$request = strtolower(trim($request, '/'));
//var_dump($request);
$route = new \Chease\Route($request);
//var_dump($route);
$route->performRoute();
