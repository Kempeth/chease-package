<?php
namespace Chease
{
	class Auth
	{
		private static $session_lifetime = 15 * 60;
		private static $remember_lifetime = 8 * 60 * 60;
		
		const NAME_SESSION_REFRESH = 'AUTH_REFRESH_TIMESTAMP';
		const NAME_REMEMBERME_COOKIE = 'rememberme';
		const NAME_SESSION_USERID = 'AUTH_USERID';
		
		
		
		public static function ConfigSessionLifetime($seconds)
		{
			self::$session_lifetime = $seconds;
		}
		
		public static function ConfigRememberMeLifetime($seconds)
		{
			self::$remember_lifetime = $seconds;
		}
		
		public static function GetSessionLifetime()
		{
			return self::$session_lifetime;
		}
		
		public static function GetRememberMeLifetime()
		{
			return self::$remember_lifetime;
		}
		
		
		
		public static function GetUserId()
		{
			static::EnsureSession();
			
			return $_SESSION[self::NAME_SESSION_USERID];
		}
		
		/**
		 * Ensures there is an active session. Unlike session_start this method can safely be called multiple times.
		 */
		public static function EnsureSession()
		{
			// Set the server side life time of sessions
			ini_set('session.gc_maxlifetime', self::$session_lifetime);
			// Set the client side life time for new session cookies
			session_set_cookie_params(self::$session_lifetime, APP);
				
			if (session_status() !== PHP_SESSION_ACTIVE)
			{
				session_start();
			}
		}
		
		/**
		 * Terminates the session.
		 */
		public static function InvalidateSession()
		{
			static::EnsureSession();
		
			$_SESSION = [];
				
			// Whenever you change principal you should regenerate the session id to prevent session fixing.
			session_regenerate_id();
		}
		
		/**
		 * Elevates the session from unauthenticated to loggedin.
		 * 
		 * @param unknown $userid
		 */
		public static function ElevateSession($userid)
		{
			if (defined('DEBUG')) echo "ElevateSession($userid)<br/>\n";
			static::EnsureSession();
			
			// in order to be elevated it need userid and refresh time
			$_SESSION[self::NAME_SESSION_USERID] = $userid;
			$_SESSION[self::NAME_SESSION_REFRESH] = time();
				
			// Whenever you change principal you should regenerate the session id to prevent session fixing.
			session_regenerate_id();
		}

		/**
		 * Creates or extends the lifetime of the remember me cookie.
		 *
		 * @param unknown $timeSpan the new lifetime of the cookie.
		 */
		public static function ExtendRememberMe()
		{
			if (!isset($_COOKIE[self::NAME_REMEMBERME_COOKIE]))
			{
				if (defined('DEBUG')) echo "ExtendRememberMe(): No cookie yet<br/>\n";
				//We don't have a "Remember Me" cookie yet
		
				//create a random selector to hinder an attacker in guessing other sessions
				$selector = bin2hex(static::RandomBytes(20, $secure));
		
				//create a random verificator to send to the client
				$verificator = bin2hex(static::RandomBytes(40, $secure));
		
				//hash the verificator to prevent impersonation in case of leaked database
				$hash = hash("sha256", $verificator); //sha256 is sufficient for this
		
				//save $selector + $hash on server using subclass implementation
				static::saveRememberMe($_SESSION[self::NAME_SESSION_USERID], $selector, $hash);
		
				//prepare cookie value for client
				$cookievalue = $selector . ":" . $verificator;
			}
			else
			{
				if (defined('DEBUG')) echo "ExtendRememberMe(): We have a cookie already<br/>\n";
				$cookievalue = $_COOKIE[self::NAME_REMEMBERME_COOKIE];
				
				//Extend Remember Me in database
				static::extractRememberMe($selector, $hash);
				static::saveRememberMe($_SESSION[self::NAME_SESSION_USERID], $selector, $hash);
			}
				
			// set cookie with $selector and $verificator on client
			if (defined('DEBUG')) echo "ExtendRememberMe(): extending client side cookie<br/>\n";
			setcookie(self::NAME_REMEMBERME_COOKIE, $cookievalue, time() + self::$remember_lifetime, APP);
		}
		
		/**
		 * Saves the protected remember me data.
		 * Creates a new row or extends an existing one if possible.
		 * 
		 * !!! Needs to be overridden in subclasses. !!!
		 * 
		 * @param unknown $selector The code used to find the data again.
		 * @param unknown $hash The hashed verification code to safe.
		 */
		protected static function saveRememberMe($user, $selector, $hash)
		{
			// DUMMY IMPLEMENTATION. GNDN
		}
		
		/**
		 * Determines whether or not the session is still valid.
		 *
		 * @return boolean
		 */
		public static function IsSessionElevated()
		{
			static::EnsureSession();
				
			// Both the server and the client need to have the session
			if (!isset($_SESSION[self::NAME_SESSION_REFRESH])) return false;
				
			// And the refresh must be recent enough
			if (time() > $_SESSION[self::NAME_SESSION_REFRESH] + self::$session_lifetime) return false;
			
			// And the session must be elevated / logged in
			if (!isset($_SESSION[self::NAME_SESSION_USERID])) return false;
			
			return true;
		}
		
		/**
		 * Creates or extends the lifetime of the session.
		 * 
		 * @param unknown $timeSpan the new lifetime of the session.
		 */
		public static function ExtendSession()
		{
			// Ensure we have a session
			static::EnsureSession();
			
			// Update the client side life time for existing session cookies 
			setcookie(session_name(), session_id(), time() + self::$session_lifetime, APP);
			
			// Update the refresh time in the session variable
			$_SESSION[self::NAME_SESSION_REFRESH] = time();
		}
		
		/**
		 * Determines whether or not the remember me cookie is still valid.
		 * 
		 * @return boolean
		 */
		public static function IsRememberMeValid()
		{
			if (defined('DEBUG')) echo "IsRememberMeValid(): find remember cookie<br/>\n";
			static::extractRememberMe($selector, $hash);
			
			if (defined('DEBUG')) echo "IsRememberMeValid(): find remember DbObject<br/>\n";
			$userid = static::findRememberMe($selector, $hash);
			
			if (defined('DEBUG')) echo "IsRememberMeValid(): userid = $userid<br/>\n";
			return $userid != null; 
		}
		
		/**
		 * Extracts the selector and hash from the remember me cookie in a safe way.
		 * 
		 * @param unknown $selector
		 * @param unknown $hash
		 */
		private static function extractRememberMe(&$selector, &$hash)
		{
			if (isset($_COOKIE[self::NAME_REMEMBERME_COOKIE]))
			{
				$cookievalue = $_COOKIE[self::NAME_REMEMBERME_COOKIE];
			}
			else
			{
				$cookievalue = ':';
			}
			
			$parts = explode(':', $cookievalue);
			if (count($parts) > 1)
			{
				$selector = $parts[0];
				$verificator = $parts[1];
				$hash = hash("sha256", $verificator); //sha256 is sufficient for this
			}
			else
			{
				$selector = '';
				$hash = '';
				$verificator = hash("sha256", $hash); //sha256 is sufficient for this
			}
		}
		
		/**
		 * Finds the remember me data and determines if it is still valid.
		 *
		 * !!! Needs to be overridden in subclasses. !!!
		 *
		 * @param unknown $selector The code used to find the data again.
		 * @param unknown $hash The hashed verification code to compare.
		 * @return NULL
		 */
		protected static function findRememberMe($selector, $hash)
		{
			// DUMMY IMPLEMENTATION. GNDN
			return null;
		}
		
		/**
		 * Elevates the session from unauthenticated to login using the information in the remember me cookie.
		 */
		public static function AuthenticateFromRemberMe()
		{
			if (defined('DEBUG')) echo "AuthenticateFromRemberMe()<br/>\n";
			static::extractRememberMe($selector, $hash);
			
			$userid = static::findRememberMe($selector, $hash);
			if (defined('DEBUG')) echo "AuthenticateFromRemberMe(): userid = $userid<br/>\n";
			
			if ($userid != null)
			{
				if (defined('DEBUG')) echo "AuthenticateFromRemberMe(): user found<br/>\n";
				static::EnsureSession();
				static::ElevateSession($userid);
			}
			else if (defined('DEBUG')) echo "AuthenticateFromRemberMe(): user not found<br/>\n";
		}
		
		
		
		public static function InvalidateRememberMe()
		{
			//TODO: Implement InvalidateRememberMe
			
			static::extractRememberMe($selector, $hash);
			
			static::destroyRememberMe($selector, $hash);
		}
		
		/**
		 * Finds the remember me data and destroys it.
		 *
		 * !!! Needs to be overridden in subclasses. !!!
		 *
		 * @param unknown $selector The code used to find the data again.
		 * @param unknown $hash The hashed verification code to compare.
		 */
		protected static function destroyRememberMe($selector, $hash)
		{
			// DUMMY IMPLEMENTATION. GNDN
		}
		
		
		
		public static function RandomBytes($length, &$secure)
		{
			if (function_exists('random_bytes'))
			{
				$secure = true;
				return random_bytes($length);
			}
			else
			{
				return openssl_random_pseudo_bytes($length, $secure);
			}
		}
	}
}