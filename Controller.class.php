<?php
namespace Chease
{
	abstract class Controller
	{
		protected $name;
		protected $route;
		protected $layout;
		
		public function __construct($name)
		{
			$this->name = $name;
		}
		
		/**
		 * Checks if the current user has access to the page.
		 * 
		 * @param \Chease\Route $route the Route that defines the page
		 * @return boolean true if the user has access
		 */
		public function checkAccess(\Chease\Route $route)
		{
			return true;
		}
		
		public function performRoute($route, $layout)
		{
			$this->route = $route;
			$this->layout = $layout;
			
			$this->performRouteInternal();
		}
		
		protected function performRouteInternal()
		{
			$premethodname = strtolower($_SERVER['REQUEST_METHOD']) . ucfirst($this->route->getAction());
			$methodname = 'get' . ucfirst($this->route->getAction());
			
			if (method_exists($this, $premethodname))
			{
				call_user_func(array($this, $premethodname));
			}
			else if (method_exists($this, $methodname))
			{
				call_user_func(array($this, $methodname));
			}
			else
			{
				http_response_code(404);
				echo "This page does not exist!";
				die();
			}
		}
	}
}