<?php
// This is a separate file from bootstrap.php so that it can be called from unittests

// Wire up autoloaders
function autoloader($classname)
{
	//echo "$classname<br/>\n";

	$classname = trim($classname, '\\');
	$parts = explode('\\', $classname);

	if (count($parts) == 1)
	{
		if (stristr($parts[count($parts) - 1], 'Controller') !== false)
		{
			$file = '../app/controllers/' . strtolower(str_replace('Controller', '', str_replace('\\', '/', $classname))) . '.php';
		}
	}
	else if (count($parts) > 1)
	{
		if ($parts[0] == 'Chease')
		{
			$file = '../framework/' . str_replace('Chease/', '', str_replace('\\', '/', $classname)) . '.class.php';
		}
		else if ($parts[0] == 'App')
		{
			$file = '../app/classes/' . str_replace('App/', '', str_replace('\\', '/', $classname)) . '.class.php';
		}
	}

	if (isset($file) && file_exists($file))
	{
		//echo " -> found!";
		require_once($file);
	}
	//else echo " NOT FOUND<br/>\n";
}
spl_autoload_register('autoloader');
require_once('../vendor/autoload.php');