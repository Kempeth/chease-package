<?php
namespace Chease
{
	class Layout
	{
		private $viewFile;
		private $titleSegments = [];
		private $titleSeparators = ' - ';
		private $vars = [];
		private $menus = ['breadcrumbs' => []];
		
		
		public function __construct($viewFile)
		{
			$this->viewFile = $viewFile;
			
			$this->menus['tabbedtoolbar'] = new \Chease\Menu\TabbedToolbar();
		}
		
		
		
		public function addTitleSegment($segment)
		{
			$this->titleSegments[] = $segment;
		}
		
		
		public function GetMenu($menu)
		{
			return $this->menus[$menu];
		}
		
		
		public function _($name, $value)
		{
			$this->vars[$name] = $value;
		}
		
		public function expressLanguage()
		{
			
		}
		
		public function expressHeaders()
		{
			$headers = [];
			
			$title = \Chease\Config::SiteTitle();
			$titles = [];
			if (strlen($title) > 0)
			{
				$titles = [$title];
			}
			$titles = array_merge($titles, $this->titleSegments);
			
			// Add title
			$headers[] = "<title>" . implode($this->titleSeparators, $titles) . "</title>";
			
			return implode('\n', $headers);
		}
		
		public function expressContent()
		{
			foreach ($this->vars as $varname=>$varvalue)
			{
				$$varname = $varvalue;
			}
			
			$filename = '../app/views/' . $this->viewFile;
			
			if (file_exists($filename))
			{
				include($filename);
			}
		}
		
		public function expressMenu($menu)
		{
			return $this->menus[$menu]->ToHtml();
		}
		
		public function performLayout()
		{
			$layout = $this;
			
			include('../app/layouts/html5.php');
		}
	}
}