<?php
namespace Chease
{
	class Config
	{
		private $db_host;
		private $db_schema;
		private $db_user;
		private $db_pwd;
		private $pathToRoot;
		
		private $mail_server;
		private $mail_port = 25;
		private $mail_security = null;
		private $mail_username;
		private $mail_password;
		private $mail_senderaddress;
		private $mail_sendername = 'EssenGeek Tool';

		private $chease_bootstrap;
		private $chease_sitetitle;
		private $chease_auth_session_timeout = null;
		private $chease_auth_rememberme_timeout = null;
		
		private static $me;
		private static $pdo;

		function __construct($chease, $db, $mail)
		{
			self::$me = $this;
			
			$this->chease_bootstrap = self::instantiateOrDefault($chease, 'bootstrap', '\Chease\BootstrapBase');
			$this->chease_sitetitle = isset($chease['title']) ? $chease['title'] : '';
			if (isset($chease['auth']))
			{
				if (isset($chease['auth']['sessiontimeout']))
				{
					$this->chease_auth_session_timeout = strtotime($chease['auth']['sessiontimeout'], 0);
				}
				if (isset($chease['auth']['remembermetimeout']))
				{
					$this->chease_auth_rememberme_timeout = strtotime($chease['auth']['remembermetimeout'], 0);
				}
			}
			
			$this->db_host = $db['host'];
			$this->db_schema = $db['schema'];
			$this->db_user = $db['username'];
			$this->db_pwd = $db['password'];
			
			$this->mail_server = $mail['serveraddress'];
			if (isset($mail['port'])) $this->mail_port = $mail['port'];
			if (isset($mail['security'])) $this->mail_securtiy = $mail['security'];
			$this->mail_username = $mail['username'];
			$this->mail_password = $mail['password'];
			$this->mail_senderaddress = $mail['senderaddress'];
			$this->mail_sendername = $mail['sendername'];
		}
		
		static function instantiateOrDefault($arr, $key, $fallbackType)
		{
			if (!isset($arr[$key])) return new $fallbackType;
				
			try
			{
				return new $arr[$key];
			}
			catch (Exception $ex)
			{
				return new $fallbackType;
			}
		}
		
		static function staticOrDefault($arr, $key, $fallbackType)
		{
			if (!isset($arr[$key])) return $fallbackType;
				
			try
			{
				$foo = new $arr[$key];
				return $arr[$key];
			}
			catch (Exception $ex)
			{
				return $fallbackType;
			}
		}
		
		/**
		 * 
		 * @return \Chease\BootstrapBase
		 */
		public static function Bootstrap()
		{
			return self::$me->chease_bootstrap;
		}
		
		public static function SiteTitle()
		{
			return self::$me->chease_sitetitle;
		}
		
		public static function SessionTimeout()
		{
			return self::$me->chease_auth_session_timeout;
		}
		
		public static function RememberMeTimeout()
		{
			return self::$me->chease_auth_rememberme_timeout;
		}
		
		public static function MailServer()
		{
			return self::$me->mail_server;
		}
		
		public static function MailPort()
		{
			return self::$me->mail_port;
		}
		
		public static function MailSecurity()
		{
			return self::$me->mail_security;
		}
		
		public static function MailUsername()
		{
			return self::$me->mail_username;
		}
		
		public static function MailPassword()
		{
			return self::$me->mail_password;
		}
		
		public static function MailSenderAddress()
		{
			return self::$me->mail_senderaddress;
		}
		
		public static function MailSenderName()
		{
			return self::$me->mail_sendername;
		}
		
		/**
		 * Logs into the database using the PDO library or returns the singleton
		 * connection of a previous log in.
		 * @return PDO the database connection
		 */
		static function DbLoginPdo()
		{
			if (!isset(self::$pdo))
			{
				self::$pdo = new \PDO(
						'mysql:host=' . self::$me->db_host . ';dbname=' . self::$me->db_schema . ';charset=UTF8',
						self::$me->db_user,
						self::$me->db_pwd,
						array(
								\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8';"
						)
				);
			}
			return self::$pdo;
		}
		
		static function FromIniContents($array)
		{
			new Config(
					$array['chease']
					, $array['db']
					, $array['mail']
					);
			
			//var_dump(self::$me);
			
			return self::$me;
		}
		
		static function Load()
		{
			$files = scandir('../config');
			$files = array_filter($files, function($file) {
				return preg_match('/config\..*\.ini/', $file);
			});
			
			if (count($files) == 1)
			{
				$file = '../config/' . array_values($files)[0];
				
				//echo $file;
				$ini = parse_ini_file($file, true);
				Config::FromIniContents($ini);
			}
			else
			{
				echo "None or too many configuration files found";
				die("None or too many configuration files found");
			}
		}

	}
}