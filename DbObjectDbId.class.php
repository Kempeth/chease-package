<?php
namespace Chease
{
	abstract class DbObjectDbId extends DbObject
	{
		protected $dbid = 0;
		
		
		/**
		 * Returns the object's database id.
		 * 
		 * @return int
		 */
		public function DbId()
		{
			return $this->dbid;
		}
		
		/**
		 * Sets the object's temporary database id.
		 * 
		 * @param int $dbid
		 */
		public function TempDbId($dbid)
		{
			$this->dbid = $dbid;
		}
		
		
		/**
		 * Saves the object to the database, choosing automatically between insert and update based on the database id.
		 * 
		 * @return unknown
		 */
		public function Save()
		{
			if (isset($this->dbid) && $this->dbid > 0)
			{
				return $this->doUpdate();
			}
			else
			{
				return $this->doInsert();
			}
		}
		
		/**
		 * Updates the object in the database
		 */
		protected abstract function doUpdate();
		
		/**
		 * Inserts the object into the database
		 */
		protected abstract function doInsert();
		
		/**
		 * Deletes the object from the database if the database id indicates an existing record.
		 */
		public function Delete()
		{
			if (isset($this->dbid) && $this->dbid > 0)
			{
				return $this->doDelete();
			}
			else
			{
				return true;
			}
		}
		
		/**
		 * Deletes the object from the database
		 */
		protected abstract function doDelete();
		
		
		
		/**
		 * Turns an array of associative array into an array of objects indexed by the object's database id.
		 * 
		 * @param unknown $assocArrayArray
		 * @return \Chease\DbObjectDbId[]
		 */
		protected static function createObjectArray($assocArrayArray)
		{
			$objArray = [];
			foreach ($assocArrayArray as $assocArray)
			{
				$obj = new static($assocArray);
				$objArray[$obj->dbid] = $obj;
			}
			return $objArray;
		}
	}
}