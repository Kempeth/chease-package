<?php
namespace Chease
{
	class Route
	{
		private $controller;
		private $action;
		
		
		public function __construct($controller = 'index', $action = 'index')
		{
			if (strcmp($controller, '') === 0)
			{
				$this->controller = 'index';
				$this->action = $action;
			}
			else if (strpos($controller, '/') !== false)
			{
				// route supplied as path
				$parts = explode('/', trim($controller,'/'));
				
				$this->controller = $parts[0];
				$this->action = $parts[1];
			}
			else
			{
				$this->controller = $controller;
				$this->action = $action;
			}
		}
		
		public function getController()
		{
			return $this->controller;
		}
		
		public function getAction()
		{
			return $this->action;
		}
		
		public function performRoute()
		{
			$fileCtrl = '../app/controllers/' . $this->controller . '.php';
			$fileView = $this->controller . '/' . $this->action . '.php';
			
			$hasCtrl = file_exists($fileCtrl);
			$hasView = file_exists('../app/views/' . $fileView);
			
			$ctrl;
			if (!$hasCtrl)
			{
				$ctrl = new HtmlController($this->controller);
			}
			else
			{
				$className = ('\\'. ucfirst($this->controller) . 'Controller');
				$ctrl = new $className($this->controller);
			}
			
			$layout = new Layout($fileView);
			
			\Chease\Config::Bootstrap()->OnLayout($layout);
			
			if ($ctrl->checkAccess($this))
			{
				$ctrl->performRoute($this, $layout);
				
				$layout->performLayout();
			}
		}
	}
}