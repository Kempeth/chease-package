<?php
namespace Chease
{
	class DbObject
	{
		const MYSQL_DUPLICATE_KEY = 1062;
		const SQL_INTEGRITY_CONSTRAINT_VIOLATION = '23000';
		
		/**
		 * Initialized the object with an array containing keys with the same names
		 * as this objects properties.
		 * @param string $array
		 */
		public function __construct($array = null)
		{
			if (is_array($array))
			{
				foreach ($array as $key=>$value)
				{
					$this->{$key} = $value;
				}
			}
		}
	}
}